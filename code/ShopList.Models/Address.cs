﻿using System;
using System.ComponentModel;

namespace ShopList.Models
{
    /// <summary>
    /// Address est une classe décrivant une adresse pour la classe shop
    /// </summary>
    public class Address : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        private int? number;

        /// <summary>
        /// Numéro de rue
        /// </summary>
        public int? Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }

        private string street;

        /// <summary>
        /// Rue
        /// </summary>
        public string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
                OnPropertyChanged("Street");
            }
        }

        private int postCode;

        /// <summary>
        /// Code postale
        /// </summary>
        public int PostCode
        {
            get
            {
                return postCode;
            }
            set
            {

                if (value < 0 || value > 99999)
                {
                    return;
                }
                postCode = value;
                OnPropertyChanged("PostCode");
            }
        }

        private string town;

        /// <summary>
        /// Ville
        /// </summary>
        public string Town
        {
            get
            {
                return town;
            }
            set
            {
                town = value;
                OnPropertyChanged("Town");
            }
        }

        public override string ToString()
        {
            return $"{Number} {Street} {PostCode} {Town}";
        }

        /// <summary>
        /// HashCode basé sur la rue et la ville
        /// </summary>
        /// <returns>Retourne le HashCode</returns>
        public override int GetHashCode()
        {
            return Street.GetHashCode() + Town.GetHashCode();
        }

        /// <summary>
        /// Définie le Equals. Une adresse est égal à une autre si chaque variable qui la constitue est égal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return this.Equals(obj as Address);
        }

        public bool Equals(Address add)
        {
            return (this.Number == add.Number) && (this.Street.ToUpper().Equals(add.Street.ToUpper())) && (this.Town.ToLower().Equals(add.Town.ToLower()) && (this.PostCode == add.PostCode));
        }
    }
}
