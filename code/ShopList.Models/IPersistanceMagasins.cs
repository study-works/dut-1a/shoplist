﻿using System.Collections.ObjectModel;

namespace ShopList.Models
{
    /// <summary>
    /// Interface pour la persistance des magasins
    /// </summary>
    public interface IPersistanceMagasins
    {
        /// <summary>
        /// Méthode de chargement des magasins
        /// </summary>
        /// <param name="path">Chemin du fichier à charger</param>
        /// <returns>Liste de magasins chargés</returns>
        ObservableCollection<Shop> ChargerMagasin(string path);

        /// <summary>
        /// Méthode de sauvegarde des magasins
        /// </summary>
        /// <param name="shops">Liste de magasins à sauvegarder</param>
        /// <param name="path">Chemin du fichier à sauvegarder</param>
        void SauvegarderMagasin(ObservableCollection<Shop> shops, string path);
    }
}
