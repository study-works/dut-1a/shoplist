﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace ShopList.Models
{
    public class Manager : INotifyPropertyChanged, INotifyCollectionChanged
    {
        public IEnumerable<Shop> Shops => shops;

        /// <summary>
        /// Liste des magasins
        /// </summary>
        public ObservableCollection<Shop> shops;

        /// <summary>
        /// Cette variable permet d'indiquer si les magasins ont été enregistrés ou non. Passe à false dès qu'une modification est effectué.
        /// </summary>
        public bool SaveShop { get; private set; } = true;

        private Shop selectedShop; 

        /// <summary>
        /// Magasin selectionné dans la listbox de l'IHM
        /// </summary>
        public Shop SelectedShop
        {
            get { return selectedShop; }
            set
            {
                selectedShop = value;
                OnPropertyChanged("SelectedShop");
            }
        }

        /// <summary>
        /// Passe à "true" si lors d'un chargement des magasins vont se retrouver en double
        /// </summary>
        public bool Doublons;

        /// <summary>
        /// Permet de savoir quelle persistance est utilisée
        /// </summary>
        private IPersistanceMagasins Pers;

        /// <summary>
        /// Création d'un nouveau magasin
        /// </summary>
        /// <param name="shop">Le nouveau magasin</param>
        public void NewShop(Shop shop)
        {
            if(!shops.Contains(shop))
            {
                shops.Add(shop);
                SaveShop = false;
            }
        }

        /// <summary>
        /// Supprime un magasin
        /// </summary>
        /// <param name="shop">Magasin à supprimer</param>
        public void RemoveShop(Shop shop)
        {
            shops.Remove(shop);
            SaveShop = false;
        }

        /// <summary>
        /// Indique la modification d'un magasin
        /// </summary>
        public void ModifyShop()
        {
            SaveShop = false;
        }

        /// <summary>
        /// Charge des magasins
        /// </summary>
        /// <param name="path">Chemin du fichier à charger</param>
        public void ChargerMagasin(string path)
        {
            ObservableCollection<Shop> LoadChops = new ObservableCollection<Shop>();
            LoadChops = Pers.ChargerMagasin(path);
            foreach (Shop shop in LoadChops)
            {
                if (!shops.Contains(shop))
                {
                    shops.Add(shop);
                }
                else
                {
                    Doublons = true;
                }
            } 
        }

        /// <summary>
        /// Sauvegarde des magasins
        /// </summary>
        /// <param name="shops">Liste des magasins à sauvegarder</param>
        /// <param name="path">Chemin du fichier à sauvegarder</param>
        public void SauvegarderMagasin(ObservableCollection<Shop> shops, string path)
        {
            Pers.SauvegarderMagasin(shops, path);
            SaveShop = true;
        }

        /// <summary>
        /// Constructeur de magasin
        /// </summary>
        /// <param name="p">Type de persistance</param>
        public Manager(IPersistanceMagasins p)
        {
            Pers = p;
            shops = new ObservableCollection<Shop>();
            //shops = Pers.ChargerMagasin(null); //Pour le stub
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        void OnPropertyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
