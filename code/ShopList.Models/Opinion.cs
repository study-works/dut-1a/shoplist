﻿namespace ShopList.Models
{
    /// <summary>
    /// Avis laissé par un utilisateur
    /// </summary>
    public class Opinion
    {
        /// <summary>
        /// Pseudo de la personne qui laisse son avis
        /// </summary>
        public string Pseudo { get; set; }

        /// <summary>
        /// Note laissée par la personne
        /// </summary>
        public int? Rating { get; set; }

        /// <summary>
        /// Commentaire de la personne
        /// </summary>
        public string Commentary { get; set; }

        /// <summary>
        /// Constructeur d'un avis
        /// </summary>
        /// <param name="pseudo">Pseudo de l'utilisateur</param>
        /// <param name="rating">Note donnée par l'utilisateur</param>
        /// <param name="commentary">Commetaire laissé par l'utilisateur</param>
        public Opinion(string pseudo, int? rating, string commentary)
        {
            Pseudo = pseudo;
            Rating = rating;
            Commentary = commentary;
        }

        public override string ToString()
        {
            return $"{Pseudo}\n{Rating}\n{Commentary}";
        }
    }
}
