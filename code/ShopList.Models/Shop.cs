﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace ShopList.Models
{
    /// <summary>
    /// Représente un magasin
    /// </summary>
    public class Shop : INotifyPropertyChanged, INotifyCollectionChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Nom du magasin
        /// </summary>
        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }


        /// <summary>
        /// Adresse du magasin
        /// </summary>
        private Address address;

        public Address Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                OnPropertyChanged("Address");
            }
        }

        /// <summary>
        /// Description du magasin
        /// </summary>
        private string description;

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }


        /// <summary>
        /// Site web du magasin
        /// </summary>
        private string webSite;

        public string WebSite
        {
            get
            {
                return webSite;
            }
            set
            {
                webSite = value;
                OnPropertyChanged("WebSite");
            }
        }


        /// <summary>
        /// Numéro de téléphone du magasin
        /// </summary>
        private string phoneNumber;

        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
                OnPropertyChanged("PhoneNumber");
            }
        }


        /// <summary>
        /// Lien vers la photo du magasin
        /// </summary>
        private string image;

        public string Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }
        }

        private bool? handicappedBool;

        /// <summary>
        /// Indique si le magasin est accessible aux handicapés
        /// </summary>
        public bool? HandicappedBool
        {
            get
            {
                return handicappedBool;
            }
            set
            {
                handicappedBool = value;
                HandicappedSet();
                OnPropertyChanged("HandicappedBool");
            }
        }

        /// <summary>
        /// Lien vers l'image "Handicappé". Si null, permet de ne pas afficher l'image
        /// </summary>
        public string Handicapped { get; set; }

        /// <summary>
        /// Modifie le lien pour l'image de handicapé
        /// </summary>
        public void HandicappedSet()
        {
            if (HandicappedBool == true)
            {
                Handicapped = "icones/handicapped.png";
            }
            else
            {
                Handicapped = null;
            }
        }

        private bool? parkingBool;

        /// <summary>
        /// Indique si un parking est disponible pour le magasin
        /// </summary>
        public bool? ParkingBool
        {
            get
            {
                return parkingBool;
            }
            set
            {
                parkingBool = value;
                ParkingSet();
                OnPropertyChanged("ParkingBool");
            }
        }

        /// <summary>
        /// Lien vers l'image "Parking". Si null, permet de ne pas afficher l'image
        /// </summary>
        public string Parking { get; set; }

        /// <summary>
        /// Modifie le lien pour l'image de handicapé
        /// </summary>
        public void ParkingSet()
        {
            if (ParkingBool == true)
            {
                Parking = "icones/parking.png";
            }
            else
            {
                Parking = null;
            }
            
        }

        private List<Hours> hours;

        /// <summary>
        /// Liste des horraires du magasin
        /// </summary>
        public List<Hours> Hours
        {
            get
            {
                return hours;
            }
            set
            {
                hours = value;
                OnPropertyChanged("Hours");
            }
        }

        public IEnumerable<Opinion> Opinions => opinions;

        /// <summary>
        /// Liste des avis laissés par les utilisateurs
        /// </summary>
        public ObservableCollection<Opinion> opinions = new ObservableCollection<Opinion>();

        private float? note;

        /// <summary>
        /// Note moyenne des utilisateurs
        /// </summary>
        public float? Note
        {
            get
            {
                return note;
            }
            set
            {
                note = value;
                OnPropertyChanged("Note");
            }
        }

        /// <summary>
        /// Calcul de la note moyenne des utilisateurs
        /// </summary>
        public void calculNote()
        {
            if (Opinions.Count() != 0)
            {
                float? totalNote = 0;
                foreach (Opinion opinion in Opinions)
                {
                    totalNote += opinion.Rating;
                }
                Note = totalNote / Opinions.Count();
            }
            else
            {
                Note = null;
            }
        }

        public override string ToString()
        {
            return $"{Name}";
        }

        /// <summary>
        /// Constructeur d'un magasin
        /// </summary>
        /// <param name="name">Nom du magasin</param>
        /// <param name="address">Adresse du magasin</param>
        /// <param name="hours">Horaires du magasin</param>
        /// <param name="description">Description du magasin</param>
        /// <param name="webSite">Site web du magasin</param>
        /// <param name="phoneNumber">Numéro de téléphone du magasin</param>
        /// <param name="image">Photo du magasin</param>
        /// <param name="shopType">Type de magasin (ex: Supermarché)</param>
        /// <param name="productType">Types de produits vendus par le magasin (ex: Alimentaire)</param>
        public Shop(string name, Address address, List<Hours> hours, string description, string webSite, string phoneNumber, bool? handicapped, bool? parking, string image)
        {
            Name = name;
            Address = address;
            Hours = hours;
            Description = description;
            WebSite = webSite;
            PhoneNumber = phoneNumber;
            HandicappedBool = handicapped;
            ParkingBool = parking;
            Image = image;
        }

        /// <summary>
        /// HashCode du magasin (sur l'adresse)
        /// </summary>
        /// <returns>Retourne le HashCode</returns>
        public override int GetHashCode()
        {
            return Address.GetHashCode();
        }

        /// <summary>
        /// Comparaison du magasin avec un objet
        /// </summary>
        /// <param name="obj">Objet (magasin à comparer</param>
        /// <returns>True si les objets sont égaux</returns>
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return this.Equals(obj as Shop);
        }

        /// <summary>
        /// Comparaison du magasin avec un autre magasin
        /// </summary>
        /// <param name="mag">L'autre magasin à comparer</param>
        /// <returns>True si les magasins sont égaux</returns>
        public bool Equals(Shop mag)
        {
            return (this.Name.Equals(mag.Name)) && (this.Address.Equals(mag.Address));
        }
    }
}
