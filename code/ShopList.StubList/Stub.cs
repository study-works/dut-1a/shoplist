﻿using ShopList.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace StubLib
{
    /// <summary>
    /// Stub permettant les tests sans persistance
    /// </summary>
    public class Stub : IPersistanceMagasins
    {
        /// <summary>
        /// Méthode de chargement des magasins
        /// </summary>
        /// <param name="path">Chemin du fichier à charger</param>
        /// <returns>Liste de magasins chargés</returns>
        public ObservableCollection<Shop> ChargerMagasin(string path)
        {
            ObservableCollection<Shop> ShopList = new ObservableCollection<Shop>();

            Shop BioAuvergne = new Shop("Bio Auvergne", new Address { Number = 8, Street = "Rue Niel", Town = "Clermont-Ferrand", PostCode = 63100 }, new List<Hours> { new Hours("Lundi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Mardi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Mercredi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Jeudi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Vendredi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Samedi", "8", "30", "12", "30", "14", "05", "18", "00") }, "Depuis 1997, nous vous accueillons dans notre magasin d'alimentation biologique.", "http://www.bio-auvergne.com/", "0473141990", true, true, "https://static4.pagesjaunes.fr/media/cviv/07963118_N_0002_photo.jpg");
            BioAuvergne.HandicappedSet();
            BioAuvergne.ParkingSet();
            Shop AuchanCebazat = new Shop("Auchan Cébazat", new Address { Number = 20, Street = "Route de Châteaugay", Town = "Cébazat", PostCode = 63118 }, new List<Hours> { null }, "Auchan Supermarché vous accompagne avec une offre qui évolue régulièrement pour mieux répondre à vos attentes. Nous vous offrons le meilleur d'Auchan tout près de chez vous.", "https://www.auchan.fr/", "0473872120", true, true, "http://www.cebazat-commerce-artisanat.fr/wp-content/uploads/2015/11/Cebazat-Simply.jpg");
            AuchanCebazat.HandicappedSet();
            AuchanCebazat.ParkingSet();
            Shop Cyclable = new Shop("Cyclable", new Address { Number = 76, Street = "Boulevard François Mitterrand", Town = "Clermont-Ferrand", PostCode = 63000 }, new List<Hours> { null }, "NC", "https://www.cyclable.com/", "0983335570", true, false, "https://www.cyclable.com/wp-content/uploads/2012/09/SAM_1069-1024x592.jpg");
            Cyclable.HandicappedSet();
            Cyclable.ParkingSet();
            Shop Decathlon = new Shop("Décathlon", new Address { Number = 1, Street = "Rue de l'Hermitage", Town = "Clermont-Ferrand", PostCode = 63000 }, new List<Hours> { null }, "Magasin de sport généraliste", "https://www.decathlon.fr/", "0473273614", true, true, "https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/36283124_1855905077802108_6033419299773743104_o.jpg?_nc_cat=104&_nc_ht=scontent-cdg2-1.xx&oh=4925b8ea533a8966d429db7059dd5e5a&oe=5D9C83AB");
            Decathlon.HandicappedSet();
            Decathlon.ParkingSet();

            BioAuvergne.opinions = new ObservableCollection<Opinion> { new Opinion ("Follombre", 10, "Bio AUvergne est un super magasin Bio"), new Opinion("Testeur", 5, "Bio AUvergne, magasin Bio pas trop mal") };
            BioAuvergne.calculNote();

            ShopList.Add(BioAuvergne);
            ShopList.Add(AuchanCebazat);
            ShopList.Add(Cyclable);
            ShopList.Add(Decathlon);

            return ShopList;
        }

        /// <summary>
        /// Méthode de sauvegarde des magasins. Non utilisé pour le stub
        /// </summary>
        /// <param name="shops">Liste de magasins à sauvegarder</param>
        /// <param name="path">Chemin du fichier à sauvegarder</param>
        public void SauvegarderMagasin(ObservableCollection<Shop> magasins, string path)
        {
            throw new NotImplementedException();
        }
    }
}
