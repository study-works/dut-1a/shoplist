﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopList.Models;
using StubLib;

namespace ShopList.Tests.TestManager
{
    [TestClass]
    public class TestManager
    {
        [TestMethod]
        public void LoadProductsTest()
        {
            Manager Mgr = new Manager(new Stub());
            Mgr.ChargerMagasin(null);
            var exist = Mgr.Shops.Contains(new Shop("Bio Auvergne", new Address { Number = 8, Street = "Rue Niel", Town = "Clermont-Ferrand", PostCode = 63100 }, new List<Hours> { new Hours("Lundi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Mardi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Mercredi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Jeudi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Vendredi", "7", "30", "12", "00", "13", "30", "17", "45"), new Hours("Samedi", "8", "30", "12", "30", "14", "05", "18", "00") }, "Depuis 1997, nous vous accueillons dans notre magasin d'alimentation biologique.", "http://www.bio-auvergne.com/", "0473141990", true, true, "https://static4.pagesjaunes.fr/media/cviv/07963118_N_0002_photo.jpg"));
            Assert.AreEqual(true, exist);
        }
    }
}
