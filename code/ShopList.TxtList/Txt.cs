﻿using ShopList.Models;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace ShopList.TxtList
{
    /// <summary>
    /// Persistance pour les magasins
    /// </summary>
    public class Txt : IPersistanceMagasins
    {
        /// <summary>
        /// Méthode de chargement des magasins
        /// </summary>
        /// <param name="path">Chemin du fichier à charger</param>
        /// <returns>Liste de magasins chargés</returns>
        public ObservableCollection<Shop> ChargerMagasin(string path)
        {
            ObservableCollection<Shop> ShopList = new ObservableCollection<Shop>();

            using (TextReader reader = File.OpenText(@path))
            {
                while (reader.Peek() > -1)
                {
                    List<Hours> Hrs = new List<Hours>();
                    ObservableCollection<Opinion> Opns = new ObservableCollection<Opinion>();
                    Shop shop = null;

                    string Nom = reader.ReadLine();
                    int.TryParse(reader.ReadLine(), out var ANumber);
                    string AStreet = reader.ReadLine();
                    int.TryParse(reader.ReadLine(), out var APostCode);
                    string ATown = reader.ReadLine();
                    for (int i = 0; i < 7; i++)
                    {
                        string Day = reader.ReadLine();
                        string ho1 = reader.ReadLine();
                        string ho2 = reader.ReadLine();
                        string ho3 = reader.ReadLine();
                        string ho4 = reader.ReadLine();
                        string ho5 = reader.ReadLine();
                        string ho6 = reader.ReadLine();
                        string ho7 = reader.ReadLine();
                        string ho8 = reader.ReadLine();
                        Hrs.Add(new Hours(Day, ho1, ho2, ho3, ho4, ho5, ho6, ho7, ho8));
                    }
                    string Desc = reader.ReadLine();
                    string Link = reader.ReadLine();
                    string Phone = reader.ReadLine();
                    bool.TryParse(reader.ReadLine(), out var Handicap);
                    bool.TryParse(reader.ReadLine(), out var Parking);
                    string Image = reader.ReadLine();
                    int.TryParse(reader.ReadLine(), out var nbOpinions);
                    for (int i = 0; i < nbOpinions; i++)
                    {
                        string Pseudo = reader.ReadLine();
                        int.TryParse(reader.ReadLine(), out var Rating);
                        string Commentary = reader.ReadLine();
                        Opns.Add(new Opinion(Pseudo, Rating, Commentary));
                    }

                    if (ANumber.ToString() == "")
                    {
                        shop = new Shop(Nom, new Address { Number = null, Street = AStreet, Town = ATown, PostCode = APostCode }, Hrs, Desc, Link, Phone, Handicap, Parking, Image);
                    }
                    else
                    {
                        shop = new Shop(Nom, new Address { Number = ANumber, Street = AStreet, Town = ATown, PostCode = APostCode }, Hrs, Desc, Link, Phone, Handicap, Parking, Image);
                    }
                    shop.HandicappedSet();
                    shop.ParkingSet();

                    shop.opinions = Opns;
                    shop.calculNote();

                    ShopList.Add(shop);

                }
            }
            return ShopList;
        }

        /// <summary>
        /// Méthode de sauvegarde des magasins
        /// </summary>
        /// <param name="shops">Liste de magasins à sauvegarder</param>
        /// <param name="path">Chemin du fichier à sauvegarder</param>
        public void SauvegarderMagasin(ObservableCollection<Shop> shops, string path)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@path))
            {
                foreach (Shop shop in shops)
                {
                    writer.WriteLine(shop.Name);
                    writer.WriteLine(shop.Address.Number);
                    writer.WriteLine(shop.Address.Street);
                    writer.WriteLine(shop.Address.PostCode);
                    writer.WriteLine(shop.Address.Town);
                    foreach (Hours hours in shop.Hours)
                    {
                        writer.WriteLine(hours.Day);
                        writer.WriteLine(hours.HourMorningOpen);
                        writer.WriteLine(hours.MinuteMorningOpen);
                        writer.WriteLine(hours.HourMorningClose);
                        writer.WriteLine(hours.MinuteMorningClose);
                        writer.WriteLine(hours.HourAfternoonOpen);
                        writer.WriteLine(hours.MinuteAfternoonOpen);
                        writer.WriteLine(hours.HourAfternoonClose);
                        writer.WriteLine(hours.MinuteAfternoonClose);
                    }
                    writer.WriteLine(shop.Description);
                    writer.WriteLine(shop.WebSite);
                    writer.WriteLine(shop.PhoneNumber);
                    writer.WriteLine(shop.HandicappedBool);
                    writer.WriteLine(shop.ParkingBool);
                    writer.WriteLine(shop.Image);
                    writer.WriteLine(shop.Opinions.Count());
                    foreach (Opinion opinion in shop.Opinions)
                    {
                        writer.WriteLine(opinion.Pseudo);
                        writer.WriteLine(opinion.Rating);
                        writer.WriteLine(opinion.Commentary);
                    }
                }
            }
        }
    }
}
