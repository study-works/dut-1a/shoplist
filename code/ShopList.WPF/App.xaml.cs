﻿using ShopList.Models;
using ShopList.TxtList;
using System.Windows;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Instanciation du manager. Ici la persistance est celle en Txt.
        /// </summary>
        public Manager Mgr { get; set; } = new Manager(new Txt());

        /// <summary>
        /// Instanciation du manager. Utilisé pour les tests avec le stub.
        /// </summary>
        //public Manager Mgr { get; set; } = new Manager(new Stub());
    }
}
