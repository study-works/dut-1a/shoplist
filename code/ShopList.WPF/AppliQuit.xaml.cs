﻿using MahApps.Metro.Controls;
using ShopList.Models;
using System.Windows;
using System.Windows.Forms;
using Application = System.Windows.Application;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour AppliQuit.xaml
    /// </summary>
    public partial class AppliQuit : MetroWindow
    {
        /// <summary>
        /// Récupère l'instanciation du manager
        /// </summary>
        public Manager Mgr => (Application.Current as App).Mgr;

        /// <summary>
        /// True s'il y a annulation
        /// </summary>
        public bool Cancel;

        public AppliQuit()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Permet de sauvegardé les magasins avant de quitter
        /// </summary>
        private void Button_Save(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveWindow = new SaveFileDialog();
            saveWindow.Filter = "Text file (*.txt)|*.txt";

            if (saveWindow.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Mgr.SauvegarderMagasin(Mgr.shops, saveWindow.FileName);
                this.Close();
            }
        }

        /// <summary>
        /// Permet de quitter sans sauvegarder
        /// </summary>
        private void Button_NoSave(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Annule le fait de quitter l'application
        /// </summary>
        private void Button_Cancel(object sender, RoutedEventArgs e)
        {
            Cancel = true;
            this.Close();
        }
    }
}
