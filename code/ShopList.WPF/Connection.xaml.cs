﻿using MahApps.Metro.Controls;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour Connection.xaml
    /// </summary>
    public partial class Connection : MetroWindow
    {
        public Connection()
        {
            InitializeComponent();
        }
    }
}
