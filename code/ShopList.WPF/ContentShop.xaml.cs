﻿using ShopList.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour ContentShop.xaml
    /// </summary>
    public partial class ContentShop : UserControl
    {
        /// <summary>
        /// Récupère l'instanciation du manager
        /// </summary>
        public Manager Mgr => (Application.Current as App).Mgr;

        public MainWindow Mw;

        /// <summary>
        /// Liste pour les notes des utilisateurs
        /// </summary>
        public List<int> RatingList { get; set; } = new List<int>();

        public ContentShop()
        {
            InitializeComponent();
            DataContext = Mgr;

            SetRatingList();
        }

        /// <summary>
        /// Instancie la liste de note de 0 à 10
        /// </summary>
        public void SetRatingList()
        {
            for (int i = 0; i <= 10; i++)
            {
                RatingList.Add(i);
            }
        }

        /// <summary>
        /// Permet d'ouvrir un lien dans un navigateur
        /// </summary>
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        /// <summary>
        /// Supprime le magasin selectionné
        /// </summary>
        private void Button_Delete(object sender, RoutedEventArgs e)
        {
            Mgr.RemoveShop(Mgr.SelectedShop);

            Mw.HomeImage();
        }

        /// <summary>
        /// Modifie le magasin selectionné
        /// </summary>
        private void Button_Modify(object sender, RoutedEventArgs e)
        {
            ModifyShop Ms = new ModifyShop();
            Ms.Mw = Mw;
            Mw.mainPart.Content = Ms;
        }

        /// <summary>
        /// Permet d'ajouter un avis
        /// </summary>
        private void Button_AddOpinion(object sender, RoutedEventArgs e)
        {
            //On regarde si le pseudo la note et le commentaire sont renseigné
            if (Pseudo.Text != "" && Commentary.Text != "" && Rating.Text != "")
            {
                Mgr.SelectedShop.opinions.Add(new Opinion(Pseudo.Text, Int32.Parse(Rating.Text), Commentary.Text));
                Mgr.SelectedShop.calculNote();
            }

            //De même, mais dans le cas où la note n'est pas renseignée
            if (Pseudo.Text != "" && Commentary.Text != "" && Rating.Text == "")
            {
                Mgr.SelectedShop.opinions.Add(new Opinion(Pseudo.Text, null, Commentary.Text));
            }

            Pseudo.Clear();
            Rating.SelectedItem = null;
            Commentary.Clear();
        }
    }
}
