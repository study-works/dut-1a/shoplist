﻿using MahApps.Metro.Controls;
using System.Windows;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour LoginInformation.xaml
    /// </summary>
    public partial class LoginInformation : MetroWindow
    {
        public LoginInformation()
        {
            InitializeComponent();
        }

        private void Button_Quit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
