﻿using MahApps.Metro.Controls;
using ShopList.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        /// <summary>
        /// Récupère l'instanciation du manager
        /// </summary>
        public Manager Mgr => (Application.Current as App).Mgr;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = Mgr;

            HomeImage();
        }

        /// <summary>
        /// Appel de l'image de l'accueil
        /// </summary>
        public void HomeImage()
        {
            var image = new Image();
            image.Source = new BitmapImage(new Uri("images/home.jpg", UriKind.Relative));
            image.Stretch = Stretch.Fill;
            Mgr.SelectedShop = null;
            mainPart.Content = image;
        }

        /// <summary>
        /// Permet de quitter l'application
        /// </summary>
        private void MenuItem_Quit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Permet d'ouvrir le chargement de magasins
        /// </summary>
        private void MenuItem_Load(object sender, RoutedEventArgs e)
        {
            if (Mgr.shops.Count() != 0)
            {
                LoadShops Ls = new LoadShops();
                Ls.Mw = this;
                Ls.ShowDialog();
            }
            else
            {
                OpenFileDialog loadWindow = new OpenFileDialog();
                loadWindow.Filter = "Text file (*.txt)|*.txt";

                if (loadWindow.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Mgr.ChargerMagasin(loadWindow.FileName);
                }
            }
        }

        /// <summary>
        /// Permet de sauvegarder les magasins
        /// </summary>
        private void MenuItem_Save(object sender, RoutedEventArgs e)
        {
            if (Mgr.shops.Count() != 0)
            {
                SaveFileDialog saveWindow = new SaveFileDialog();
                saveWindow.Filter = "Text file (*.txt)|*.txt";

                if (saveWindow.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Mgr.SauvegarderMagasin(Mgr.shops, saveWindow.FileName);

                    SaveShops saveShops = new SaveShops();
                    saveShops.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Aucun magasin à sauvegarder");
            }
        }

        /// <summary>
        /// Ajoute un magasin
        /// </summary>
        private void MenuItem_AddShop(object sender, RoutedEventArgs e)
        {
            NewShop Ns = new NewShop();
            Ns.Mw = this;
            mainPart.Content = Ns;
        }

        /// <summary>
        /// Modifie le magasin sélectionné
        /// </summary>
        private void MenuItem_ModifyShop(object sender, RoutedEventArgs e)
        {
            if (Mgr.SelectedShop != null)
            {
                ModifyShop Ms = new ModifyShop();
                Ms.Mw = this;
                mainPart.Content = Ms;
            }
            else
            {
                MessageBox.Show("Aucun magasin selectionné");
            }
        }

        /// <summary>
        /// Supprime le magasin sélectionné
        /// </summary>
        private void MenuItem_DelShop(object sender, RoutedEventArgs e)
        {
            if (Mgr.SelectedShop != null)
            {
                Mgr.RemoveShop(Mgr.SelectedShop);

                this.HomeImage();
            }
            else
            {
                MessageBox.Show("Aucun magasin selectionné");
            }
        }

        /// <summary>
        /// Permet d'ouvrir la page à propos de l'application
        /// </summary>
        private void MenuItem_About(object sender, RoutedEventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        /// <summary>
        /// Permet d'ouvrir la page d'information sur la connexion
        /// </summary>
        private void MenuItem_LoginInformation(object sender, RoutedEventArgs e)
        {
            LoginInformation li = new LoginInformation();
            li.ShowDialog();
        }

        /// <summary>
        /// Permet d'ouvrir la page de connexion
        /// </summary>
        private void MenuItem_Connexion(object sender, RoutedEventArgs e)
        {
            Connection c = new Connection();
            c.ShowDialog();
        }

        /// <summary>
        /// Retourne à l'accueil
        /// </summary>
        private void Button_Home(object sender, RoutedEventArgs e)
        {
            HomeImage();
        }

        /// <summary>
        /// Permet d'ouvrir la page de connexion
        /// </summary>
        private void Button_Login(object sender, RoutedEventArgs e)
        {
            Connection c = new Connection();
            c.ShowDialog();
        }

        /// <summary>
        /// Affichage du magasin sélectionné
        /// </summary>
        private void ListBox_Shops(object sender, SelectionChangedEventArgs e)
        {
            ContentShop Cs = new ContentShop();
            Cs.Mw = this;
            mainPart.Content = Cs;
        }

        /// <summary>
        /// Ajoute un magasin
        /// </summary>
        private void Button_AddShop(object sender, RoutedEventArgs e)
        {
            NewShop Ns = new NewShop();
            Ns.Mw = this;
            mainPart.Content = Ns;
        }

        /// <summary>
        /// Permet de proposer des options lors de la fermeture si des modifications ont été faite depuis la dernère sauvegarde
        /// </summary>
        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!Mgr.SaveShop && Mgr.shops.Count() != 0)
            {
                AppliQuit Aq = new AppliQuit();
                Aq.ShowDialog();
                if (Aq.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
