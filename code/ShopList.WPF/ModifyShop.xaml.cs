﻿using ShopList.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour ModifyShop.xaml
    /// </summary>
    public partial class ModifyShop : UserControl
    {
        /// <summary>
        /// Récupère l'instanciation du manager
        /// </summary>
        public Manager Mgr => (Application.Current as App).Mgr;

        public MainWindow Mw;

        /// <summary>
        /// Liste des heures de la journée
        /// </summary>
        public List<string> Hours { get; set; } = new List<string>();

        /// <summary>
        /// Liste des minutes d'une heure
        /// </summary>
        public List<string> Minutes { get; set; } = new List<string>();

        public ModifyShop()
        {
            InitializeComponent();
            DataContext = Mgr;

            Horaire();
        }

        /// <summary>
        /// Instancie :
        /// la liste d'heure : de 00 à 23
        /// la liste des minutes : de 00 à 55 de 5 en 5
        /// </summary>
        private void Horaire()
        {
            int i;

            Hours.Add("__"); //NC = Non connu
            for (i = 0; i <= 23; i++)
            {
                if (i < 10)
                {
                    Hours.Add(i.ToString("00"));
                }
                else
                {
                    Hours.Add(i.ToString());
                }
            }

            Minutes.Add("__"); //NC = Non connu
            for
                (i = 0; i <= 60; i = i + 5)
            {
                if (i < 10)
                {
                    Minutes.Add(i.ToString("00"));
                }
                else
                {
                    Minutes.Add(i.ToString());
                }
            }
        }

        /// <summary>
        /// Valide la modification d'un magasin
        /// </summary>
        private void Button_Validate(object sender, RoutedEventArgs e)
        {
            BindingExpression beNa = Name.GetBindingExpression(TextBox.TextProperty);
            beNa.UpdateSource();
            BindingExpression beNu = Number.GetBindingExpression(TextBox.TextProperty);
            beNu.UpdateSource();
            BindingExpression beSt = Street.GetBindingExpression(TextBox.TextProperty);
            beSt.UpdateSource();
            BindingExpression bePc = PostCode.GetBindingExpression(TextBox.TextProperty);
            bePc.UpdateSource();
            BindingExpression beTo = Town.GetBindingExpression(TextBox.TextProperty);
            beTo.UpdateSource();
            BindingExpression beWs = WebSite.GetBindingExpression(TextBox.TextProperty);
            beWs.UpdateSource();
            BindingExpression bePn = PhoneNumber.GetBindingExpression(TextBox.TextProperty);
            bePn.UpdateSource();
            BindingExpression beIm = Image.GetBindingExpression(TextBox.TextProperty);
            beIm.UpdateSource();
            BindingExpression beHb = HandicappedBool.GetBindingExpression(CheckBox.IsCheckedProperty);
            beHb.UpdateSource();
            Mgr.SelectedShop.HandicappedSet();
            BindingExpression bePb = ParkingBool.GetBindingExpression(CheckBox.IsCheckedProperty);
            bePb.UpdateSource();
            Mgr.SelectedShop.ParkingSet();
            BindingExpression beDe = Description.GetBindingExpression(TextBox.TextProperty);
            beDe.UpdateSource();

            Mgr.ModifyShop();

            ContentShop Cs = new ContentShop();
            Cs.Mw = Mw;
            Mw.mainPart.Content = Cs;
        }

        /// <summary>
        /// Annule la modification d'un magasin
        /// </summary>
        private void Button_Cancel(object sender, RoutedEventArgs e)
        {
            ContentShop Cs = new ContentShop();
            Cs.Mw = Mw;
            Mw.mainPart.Content = Cs;
        }
    }
}
