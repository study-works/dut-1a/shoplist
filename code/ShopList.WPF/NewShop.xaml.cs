﻿using ShopList.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour NewShop.xaml
    /// </summary>
    public partial class NewShop : UserControl
    {
        /// <summary>
        /// Récupère l'instanciation du manager
        /// </summary>
        public Manager Mgr => (Application.Current as App).Mgr;

        public MainWindow Mw;

        /// <summary>
        /// True si lors de l'ajout d'un magasin quelque chose n'est pas bien renseigné
        /// </summary>
        public bool Erreur { get; set; }

        /// <summary>
        /// Liste des heures de la journée
        /// </summary>
        public List<string> Hours { get; set; } = new List<string>();

        /// <summary>
        /// Liste des minutes d'une heure
        /// </summary>
        public List<string> Minutes { get; set; } = new List<string>();

        private Shop shop;

        public NewShop()
        {
            InitializeComponent();
            DataContext = this;

            Horaire();
        }

        /// <summary>
        /// Instancie :
        /// la liste d'heure : de 00 à 23
        /// la liste des minutes : de 00 à 55 de 5 en 5
        /// </summary>
        private void Horaire()
        {
            int i;

            Hours.Add("__"); //NC = Non connu
            for (i = 0; i <= 23; i++)
            {
                if (i < 10)
                {
                    Hours.Add(i.ToString("00"));
                }
                else
                {
                    Hours.Add(i.ToString());
                }
            }

            Minutes.Add("__"); //NC = Non connu
            for
                (i = 0; i <= 60; i = i + 5)
            {
                if (i < 10)
                {
                    Minutes.Add(i.ToString("00"));
                }
                else
                {
                    Minutes.Add(i.ToString());
                }
            }
        }

        /// <summary>
        /// Valide l'ajout d'un magasin
        /// </summary>
        private void Button_Validate(object sender, RoutedEventArgs e)
        {
            if (ShopName.Text == "")
            {
                Erreur = true;
            }

            Address address = null;

            if (Number.Text != "" && Street.Text != "" && Town.Text != "" && PostCode.Text != "")
            {
                address = new Address { Number = Int32.Parse(Number.Text), Street = Street.Text, Town = Town.Text, PostCode = Int32.Parse(PostCode.Text) };
            }
            else if (Number.Text == "" && Street.Text != "" && Town.Text != "" && PostCode.Text != "")
            {
                address = new Address { Number = null, Street = Street.Text, Town = Town.Text, PostCode = Int32.Parse(PostCode.Text) };
            }
            else
            {
                Erreur = true;
            }

            List<Hours> hours = new List<Hours>();

            hours.Add(new Hours("Lundi", MondayHourMorningOpen.Text, MondayMinuteMorningOpen.Text, MondayHourMorningClose.Text, MondayMinuteMorningClose.Text, MondayHourAfternoonOpen.Text, MondayMinuteAfternoonOpen.Text, MondayHourAfternoonClose.Text, MondayMinuteAfternoonClose.Text));
            hours.Add(new Hours("Mardi", TuesdayHourMorningOpen.Text, TuesdayMinuteMorningOpen.Text, TuesdayHourMorningClose.Text, TuesdayMinuteMorningClose.Text, TuesdayHourAfternoonOpen.Text, TuesdayMinuteAfternoonOpen.Text, TuesdayHourAfternoonClose.Text, TuesdayMinuteAfternoonClose.Text));
            hours.Add(new Hours("Mercredi", WednesdayHourMorningOpen.Text, WednesdayMinuteMorningOpen.Text, WednesdayHourMorningClose.Text, WednesdayMinuteMorningClose.Text, WednesdayHourAfternoonOpen.Text, WednesdayMinuteAfternoonOpen.Text, WednesdayHourAfternoonClose.Text, WednesdayMinuteAfternoonClose.Text));
            hours.Add(new Hours("Jeudi", ThursdayHourMorningOpen.Text, ThursdayMinuteMorningOpen.Text, ThursdayHourMorningClose.Text, ThursdayMinuteMorningClose.Text, ThursdayHourAfternoonOpen.Text, ThursdayMinuteAfternoonOpen.Text, ThursdayHourAfternoonClose.Text, ThursdayMinuteAfternoonClose.Text));
            hours.Add(new Hours("Vendredi", FridayHourMorningOpen.Text, FridayMinuteMorningOpen.Text, FridayHourMorningClose.Text, FridayMinuteMorningClose.Text, FridayHourAfternoonOpen.Text, FridayMinuteAfternoonOpen.Text, FridayHourAfternoonClose.Text, FridayMinuteAfternoonClose.Text));
            hours.Add(new Hours("Samedi", SundayHourMorningOpen.Text, SundayMinuteMorningOpen.Text, SundayHourMorningClose.Text, SundayMinuteMorningClose.Text, SundayHourAfternoonOpen.Text, SundayMinuteAfternoonOpen.Text, SundayHourAfternoonClose.Text, SundayMinuteAfternoonClose.Text));
            hours.Add(new Hours("Dimanche", SaturdayHourMorningOpen.Text, SaturdayMinuteMorningOpen.Text, SaturdayHourMorningClose.Text, SaturdayMinuteMorningClose.Text, SaturdayHourAfternoonOpen.Text, SaturdayMinuteAfternoonOpen.Text, SaturdayHourAfternoonClose.Text, SaturdayMinuteAfternoonClose.Text));

            if (!Erreur)
            {
                shop = new Shop(ShopName.Text, address, hours, Description.Text, WebSite.Text, PhoneNumber.Text, Handicapped.IsChecked, Parking.IsChecked, Image.Text);
                Mgr.NewShop(shop);

                Mgr.SelectedShop = shop;
                ContentShop Cs = new ContentShop();
                Cs.Mw = Mw;
                Mw.mainPart.Content = Cs;
            }

            else
            {
                NewShopError Nse = new NewShopError();
                Nse.ShowDialog();
                Erreur = false;
            }
        }

        /// <summary>
        /// Annule l'ajout d'un magasin
        /// </summary>
        private void Button_Cancel(object sender, RoutedEventArgs e)
        {
            Mgr.SelectedShop = shop;
            ContentShop Cs = new ContentShop();
            Cs.Mw = Mw;
            Mw.mainPart.Content = Cs;
        }
    }
}
