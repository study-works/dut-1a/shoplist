﻿using MahApps.Metro.Controls;
using System.Windows;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour NewShopError.xaml
    /// </summary>
    public partial class NewShopError : MetroWindow
    {
        public NewShopError()
        {
            InitializeComponent();
        }

        private void Button_Validate(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
